<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('alpha')->group(function () {

    Route::post('user', [
        'uses' => 'AuthController@store'
    ]);

    Route::post('user/signin', [
        'uses' => 'AuthController@signin'
    ]);

    Route::get('user', [
        'uses' => 'AuthController@getUser'
    ]);

    Route::patch('user/{id}', [
        'uses' => 'AuthController@editUser'
    ]);

    Route::patch('admin/user/actions/{id}', [
        'uses' => 'AuthController@suspendUser'
    ]);

    Route::resource('user/bio', 'UserInformation\BioController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('user/medi', 'MediController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hospital/visit', 'Hospital\HosVisitController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hospital/treatment', 'Hospital\TreamentController', [
        'except' => ['edit', 'create']
    ]);

    Route::get('hospital/visitspecial', [
        'uses' => 'Hospital\HosVisitSpecController@getVisitToday'
    ]);


    Route::get('attendance/chapelspec/{id}', [
        'uses' => 'Attendance\ChapelAttendanceSpecController@getFirstSemesterCount'
    ]);

    Route::get('attendance/chapelspectwo/{id}', [
        'uses' => 'Attendance\ChapelAttendanceSpecController@getSecondSemesterCount'
    ]);

    Route::get('attendance/hostelspecone/{id}', [
        'uses' => 'Attendance\HostelAttendanceSpecController@getFirstSemesterCount'
    ]);

    Route::get('attendance/hostelspectwo/{id}', [
        'uses' => 'Attendance\HostelAttendanceSpecController@getSecondSemesterCount'
    ]);

    Route::get('attendance/hostelspecday/', [
        'uses' => 'Attendance\HostelAttendanceSpecController@getDayCount'
    ]);

    Route::get('attendance/hostelspeclist/', [
        'uses' => 'Attendance\HostelAttendanceSpecController@getDayList'
    ]);

    Route::post('security/passport', [
        'uses' => 'Security\RegPassportController@savePassport'
    ]);

    Route::get('student/exeatspec/{id}', [
        'uses' => 'Student\ExeatSpecController@getExeatByMatric'
    ]);

    Route::get('student/exeatspecpending', [
        'uses' => 'Student\ExeatSpecController@getPending'
    ]);

    Route::post('student/exeatspec/{id}', [
        'uses' => 'Student\ExeatSpecController@storeMedic'
    ]);

    Route::get('student/casespec/{id}', [
        'uses' => 'Student\OffencespecController@getCaseMatric'
    ]);

    Route::resource('student/exeat', 'Student\ExeatController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('student/statement', 'Student\StatementController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('student/case', 'Student\OffenceController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('security/registration', 'Security\RegistrationController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('security/attendance', 'Security\GateMovementController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hostel/attendance', 'Attendance\HostelAttendanceController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hospital/drug/category', 'Hospital\DrugCategoryController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hospital/drug', 'Hospital\DrugController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('hospital/prescription', 'Hospital\DrugPreController', [
        'except' => ['edit', 'create']
    ]);

    Route::resource('attendance/chapel', 'Attendance\ChapelAttendanceController', [
        'except' => ['edit', 'create']
    ]);
});
