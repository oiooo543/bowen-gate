<?php

namespace App;

use App\Attendance\ChapelAttendance;
use App\Attendance\HostelAttendance;
use App\hospital\Drug;
use App\hospital\DrugCategory;
use App\hospital\DrugPre;
use App\Hospital\HosVisit;
use App\hospital\Treament;
use App\Model\Student\Exeat;
use App\Model\Student\Offence;
use App\Model\Student\Statment;
use App\Security\GateMovement;
use App\Security\SecurityRegistration;
use App\UserModel\Biodata;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mat_no', 'phone', 'type', 'status'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function statement() {
        return $this->hasMany(Statment::class);
    }

    public function offence () {
        return $this->hasMany(Offence::class);
    }

    public function exeat() {
        return $this->hasMany(Exeat::class);
    }

    public function hostelattendance() {
        return $this->hasMany(HostelAttendance::class);
    }

    public function gatemovement() {
        return $this->hasMany(GateMovement::class);
    }

    public function regsecurity() {
        return $this->hasMany(SecurityRegistration::class);
    }

    public function chapelattendance() {
        return $this->hasMany(ChapelAttendance::class);
    }

    public function drug() {
        return $this->hasMany(Drug::class);
    }

    public function drugcategory() {
        return $this->hasMany(DrugCategory::class);
    }

    public function biodata() {
        return $this->hasOne(Biodata::class);
    }

    public function medidata() {
        return $this->hasOne(MediData::class);
    }

    public function prescribes() {
        return $this->hasMany(DrugPre::class);
    }

    public function hosvisits() {
        return $this->hasMany(HosVisit::class);
    }

    public function treatment() {
        return $this->hasMany(Treament::class);
    }
}
