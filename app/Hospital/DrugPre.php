<?php

namespace App\hospital;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DrugPre extends Model
{
    protected $fillable = ['patient_id', 'drug_g', 'bill', 'diagnosis' ];


    public function user() {
        return $this->belongsTo(User::class);
    }
}
