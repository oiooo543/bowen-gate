<?php

namespace App\Hospital;

use App\User;
use Illuminate\Database\Eloquent\Model;

class HosVisit extends Model
{
    protected $fillable = [
        'phone', 'type', 'status'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function treatment() {
        return $this->hasOne(Treament::class);
    }
}
