<?php

namespace App\hospital;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use function Symfony\Component\Debug\Tests\testHeader;

class Treament extends Model
{
    protected $fillable = [
        'prescription', 'diagnosis', 'type', 'user_id', 'hos_visit_id', 'patient_id'
    ];

    public function hosvisit() {
        return $this->belongsTo(HosVisit::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
