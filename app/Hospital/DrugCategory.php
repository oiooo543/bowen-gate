<?php

namespace App\hospital;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DrugCategory extends Model
{
    protected $fillable = [
        'description', 'name'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function drug() {
        return $this->hasMany('drug');
    }
}
