<?php

namespace App\hospital;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Drug extends Model
{
    protected $fillable = [
        'name', 'quantity', 'description', 'category', 'used', 'balance', 'added'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function drugcategory() {
        return $this->belongsTo(DrugCategory::class);
    }
}
