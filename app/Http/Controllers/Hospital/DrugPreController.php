<?php

namespace App\Http\Controllers\hospital;

use App\hospital\DrugPre;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class DrugPreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('You are not authorized', 401);
        }

        $patients = DrugPre::with('user')->get()->sortByDesc('created_by');

        //  return $patients;


        foreach ($patients as $patient){

            $patient_id = $patient['patient_id'];

            $details = User::all()->where('id', $patient_id)->first();

            //  return $details;

            $mat_no = $details['mat_no'];


            $pres = array(

                'id'=> $patient['id'],
                'name' => $patient['user']['name'],
                'matric' => $mat_no,
                'phone' => $patient['user']['phone'],
                'drug_g' => $patient['drug_g'],
                'diagnosis' => $patient['diagnosis'],
                'time' => date($patient['created_at']),

            );
            $pat [] =$pres;
        }



        if ($patients->isEmpty()){
            return response()->json($pres, 401);
        }else{
            return response()->json($pat, 200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('You are not authorized', 404);
        }

        $drugpres = new DrugPre($request->all());

        if ($user->prescribes()->save($drugpres)){
            return response()->json('Your precription was succesful', '200');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('You are not authorized', 404);
        }

        $personals = DrugPre::with('user')->get()->sortByDesc('create_at')->where('patient_id', $id)->toArray();

        if ($personals == null ){
            return response()->json('record not found', 404);
        }

      //  return $personals;

        foreach ($personals as $personal){
            $tobi = array('name' => $personal['user']['name'],
                'mat_no' =>  $personal['user']['mat_no'],
                'drug_g' => $personal['drug_g'],
                'diagnosis' => $personal['diagnosis'],
                'time' => date($personal['created_at']),
            );

            $pet [] = $tobi;
        }


        return response()->json($pet, '200');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
