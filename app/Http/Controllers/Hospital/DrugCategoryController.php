<?php

namespace App\Http\Controllers\hospital;

use App\hospital\DrugCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class DrugCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $category = DrugCategory::all();

        if ($category != null ) {

            return response()->json($category, 200);
        } else {
            return response()->json('No list', 204);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $drugcategory = new DrugCategory($request->all());

        if ($user->drugcategory()->save($drugcategory)) {
            return response()->json('Category created', 200);
        } else {
            return response()->json('category creation failed', 402);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $drugcategory = DrugCategory::find($id);

        if ($drugcategory != null ){
            return response()->json($drugcategory, 200);
        } else {
            return response()->json('no data', 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $drugcategory = DrugCategory::find($id);

        if ($drugcategory->update($request->all())){
            return response()->json('Update successful', 200);
        }else{
            return response()->json('update not successful', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
