<?php

namespace App\Http\Controllers\Hospital;

use App\Hospital\HosVisit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class HosVisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $hosvisits = HosVisit::with('user')->get()->sortByDesc('created_at', SORT_DESC);

        $visits = null;
        foreach ($hosvisits as $hosvisit){
            $visit = array('visit_id'=> $hosvisit['id'], 'user' => $hosvisit['user'], 'status' => $hosvisit['status']);

            $visits [] = $visit;
        }



        if ($hosvisits != null) {
            return response()->json($visits, 200);
        } else {
            return response()->json('No Content', 204);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $hosvisit = new HosVisit($request->all());

        if ($user->hosvisits()->save($hosvisit)){


            return response()->json('You are now on the queue', '201');
        }else{
            return response()->json('Not successful', '400');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $visits = HosVisit::all()->where('phone', $id)->sortByDesc('created_at');

        if ($visits != null){
            return response()->json($visits, 200);
        }else {
            return response()->json('no content', 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
