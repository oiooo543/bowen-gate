<?php

namespace App\Http\Controllers\hospital;

use App\Hospital\HosVisit;
use App\hospital\Treament;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class TreamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $treaments = Treament::with('user')->get()->sortByDesc('created_at', SORT_DESC);

        $treats = null;
        foreach ($treaments as $treament) {
            $treats [] = array('id' => $treament['id'], 'prescription' => $treament['prescription'], 'diagnosis'=> $treament['diagnosis'],
                'date'=> $treament['created_at']->format('d-m-y'), 'doctor' => $treament['user']['name']);
        }

        if ($treats != null) {
            return response()->json($treats, 200);
        } else {
            return response ()->json('No content', 204);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        if($user->type != 'doc' ){
            return response()->json('You are not authorized');
        }

        $hosvit = HosVisit::find($request->input('hos_visit_id'));



        $treatment = new Treament($request->all());

        if ($user->treatment()->save($treatment)) {

            $hosvit->status = 'Treated';

            if ($hosvit->save()){
                return response()->json('Treatment Successful', 200);
            } else {
                return response()->json('Save successful, update failed', 202);
            }


        } else {
            return response()->json('save failed');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $treatments = Treament::with('user')->get()->where('patient_id', $id);

        //return $treatments;
        $treats = null;
        foreach ($treatments as $treament) {
            $treats [] = array('id' => $treament['id'], 'prescription' => $treament['prescription'], 'diagnosis'=> $treament['diagnosis'],
                'date'=> $treament['created_at']->format('d-m-y'), 'doctor' => $treament['user']['name'], 'type'=> $treament['type']);
        }

        if ($treats != null) {
            return response()->json($treats, 200);
        } else {
            return response ()->json('No content', 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $patient = Treament::find($id);

        if ($patient != null ) {

            $patient->type = "Discharged";

            if ($user->treatment()->save()){
                return response()->json('Patient Discharged', 200);
            } else {
                return response()->json('discharge failed', 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
