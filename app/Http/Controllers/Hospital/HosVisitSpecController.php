<?php

namespace App\Http\Controllers\Hospital;

use App\Hospital\HosVisit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class HosVisitSpecController extends Controller
{
    public function getVisitTodayNotTreated()
    {

        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json('User not registered', 404);
        }

        $time = Carbon::now();

        return $time;

        $visits = HosVisit::all()->where('created_at', $time)->where('status', 'not treated');

        if ($visits != null) {

            return response()->json($visits, 200);
        } else {
            return response()->json('no content', 204);
        }

    }
    public function getVisitToday()
    {

        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json('User not registered', 404);
        }

        $time = Carbon::now()->toDateString();

      // return $time;

        $visits = HosVisit::whereDate('created_at', $time)->get();

        if ($visits != null) {

            return response()->json($visits, 200);
        } else {
            return response()->json('no content', 204);
        }
    }
}
