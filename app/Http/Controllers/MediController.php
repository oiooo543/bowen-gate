<?php

namespace App\Http\Controllers;

use App\MediData;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class MediController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization Failed', 401);
        }

        //return $user->type;

        if($user->type != 'doc' ){
            return response()->json('You are not authorized');
        }

        $med = new MediData($request->all());


        if ($user->medidata()->save($med)){
            return response()->json('Medical Profile creation successful', 201);
        }else{
            return response()->json('Profile creation failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MediData  $mediData
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('Authorization Failed', 401);
        }

        $med = MediData::all()->where('mat_no', $id)->first();

        if ($med != null) {
            return response()->json($med, 200);
        }else {
            return response()->json('This is empty', 204);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MediData  $mediData
     * @return \Illuminate\Http\Response
     */
    public function edit(MediData $mediData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MediData  $mediData
     * @return \Illuminate\Http\Response
     */
    public function update($mat_no, Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        if($user->type != 'doc' ){
            return response()->json('You are not authorized');
        }

        $ownid = User::all()->where('mat_no', $mat_no)->first()->pluck('id');

        $med = MediData::where('user_id', $ownid);

        if ($med != null ) {
            if ($user->medidata()->update($request->all())){
                return response()->json('update successful', 202);
            }else{
                return response()->json('update failed', 401);
            }
        }else{
            return response()->json('bio not found', 404);
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MediData  $mediData
     * @return \Illuminate\Http\Response
     */
    public function destroy(MediData $mediData)
    {
        //
    }
}
