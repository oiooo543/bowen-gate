<?php

namespace App\Http\Controllers\Security;

use App\Security\GateMovement;
use App\Security\SecurityRegistration;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class GateMovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $movement  = GateMovement::all()->get();

        if ($movement != null ){
            return response()->json($movement, 200);
        }else {
            return response()->json('List empty', 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $check = SecurityRegistration::find($request->input('security_id'));

        if($check == null){
            return response()->json('User not found', 404);
        }

        $attendance = new GateMovement($request->all());

        if($user->gatemovement()->save($attendance)) {
            return response()->json('attendance marked', 200);
        } else {
            return response()->json('data save failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $movements = GateMovement::all()->where('$id', $id)->sortByDesc('created_at', SORT_DESC)->get();

        if ($movements != null ){
            return response()->json($movements, 200);
        } else {
            return response()->json('No data found', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
