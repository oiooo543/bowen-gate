<?php

namespace App\Http\Controllers\Security;

use App\Security\SecurityRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;


class RegPassportController extends Controller
{
    public function savePassport(Request $request) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:50480',
        ]);


        $image = $request->file('avatar');
        $name = $request->input('phone').'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $name);

      //  $this->save();
        $path = 'http://localhost/BowenGate/public/images'.'/'.$name;

        $details = SecurityRegistration::find($request->input('id'));

        $details->path = $path;

       // return $details;

        if ($details  != null ) {

            if ($details->save()){
                return response()->json($path, 200);
            }

            else {
                return response()->json('Update failed', 404);
            }
        }


    }
}
