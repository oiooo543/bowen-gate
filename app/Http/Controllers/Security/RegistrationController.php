<?php

namespace App\Http\Controllers\Security;

use App\Security\SecurityRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $secregistration = SecurityRegistration::all()->sortByDesc('created_at', SORT_DESC);

        if ($secregistration != null) {
            return response()->json($secregistration, 200);
        } else {
            return response()->json('not found', 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $registration = new SecurityRegistration($request->all());

        if ($user->regsecurity()->save($registration)) {
            return response()->json('Data saved', 200);
        } else {
            return response()->json('Data save failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($phone)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $registration = SecurityRegistration::where('phone', $phone)->get();

        if ($registration != null ){
            return response()->json($registration, 200);
        } else {
            return response()->json('No data found', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $register = SecurityRegistration::find($id);

        if ($register->update($request->all())) {
            return response()->json('update successful', 200);
        } else {
            return response()->json('update failed', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
