<?php

namespace App\Http\Controllers\UserInformation;

use App\UserModel\Biodata;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class BioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }




        $biodata = new Biodata($request->all());

        //

        if ($user->biodata()->save($biodata)){

            return response()->json('profile created', '201');
        }else{
            return response()->json('profile creation failed', '400');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $bio = BioData::all()->where('user_id', $id)->first();



        if ($bio != null) {
            return response()->json($bio, 200);
        } else {
            return response()->json('please fill your Bio data', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $bio = BioData::where('id', $id);

        if ($bio != null){
            if ($user->biodata()->update($request->all())){
                return response()->json('update successful', 202);
            }else{
                return response()->json('update failed', 401);
            }
        }else{
            return response()->json('bio not found', 404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
