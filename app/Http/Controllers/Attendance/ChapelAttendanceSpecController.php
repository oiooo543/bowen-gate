<?php

namespace App\Http\Controllers\Attendance;

use App\Attendance\ChapelAttendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class ChapelAttendanceSpecController extends Controller
{

    public function getFirstSemesterCount($mat_no) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $now = Carbon::now();

        $attendance = ChapelAttendance::where('semester', '1')->whereYear('created_at', $now->year)->
                    where('mat_no', $mat_no)->get()->count();

        if ($attendance != null) {
            return response()->json($attendance, 200);
        } else {
            return response()->json('No data found', 404);
        }
    }

    public function getSecondSemesterCount($mat_no) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $now = Carbon::now();

        $attendance = ChapelAttendance::where('semester', '2')->whereYear('created_at', $now->year)->
                    where('mat_no', $mat_no)->get()->count();

        if ($attendance != null) {
            return response()->json($attendance, 200);
        } else {
            return response()->json('No data found', 404);
        }
    }
}
