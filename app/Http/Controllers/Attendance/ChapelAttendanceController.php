<?php

namespace App\Http\Controllers\Attendance;

use App\Attendance\ChapelAttendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class ChapelAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $attendances = ChapelAttendance::all()->sortByDesc('created_at', SORT_DESC);

        $attend = null;
        foreach ($attendances as $attendance) {
            $attend [] = $attendance;
        }

        if ($attendances != null ) {
            return response()->json($attend, 200);
        } else {
            return response()->json('No data found', 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        if ($user->type != 'officer'){
            return response()->json('you not authorized', 404);
        }

        $attendance = new ChapelAttendance($request->all());

        if ($user->chapelattendance()->save($attendance)) {
            return response()->json('attendance marked', 200);
        }else {
            return response()->json('attendance not marked', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $attendance = ChapelAttendance::all()->where('mat_no', $id);

        if ($attendance->isNotEmpty()){
            return response()->json($attendance, 200);
        }else {
            return response()->json('No content found', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
