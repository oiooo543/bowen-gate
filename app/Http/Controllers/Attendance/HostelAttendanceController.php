<?php

namespace App\Http\Controllers\Attendance;

use App\Attendance\HostelAttendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  JWTAuth;

class HostelAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $details = HostelAttendance::all();

        if ($details != null ){
            return response()->json($details, 200);
        } else {
            return response()->json('No data found', 204);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $attendance = new  HostelAttendance($request->all());

        if ($user->hostelattendance()->save($attendance)){
            return response()->json('attendance marked', 200);
        }else{
            return response()->json('data not saved', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $details = HostelAttendance::where('mat_no', $id)->get()->sortByDesc('created_at', 3);

        $det = null;
        foreach ($details as $detail) {
            $det [] = $detail;
        }

        if ($details != null ) {
            return response()->json($det, 200);
        }else {
            return response()->json('No data found', 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
