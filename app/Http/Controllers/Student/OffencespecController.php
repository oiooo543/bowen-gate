<?php

namespace App\Http\Controllers\Student;

use App\Model\Student\Offence;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class OffencespecController extends Controller
{

    public function getCaseMatric($mat_no) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $cases = Offence::all()->where('mat_no', $mat_no)->sortByDesc('created_at', 3);

        if ($cases != null ) {
            return response()->json($cases , 200);
        } else {
            return response()->json("You must be a saint");
        }


    }


    public function getCategoryCount($category) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $now = Carbon::now();

        $categoryCount = Offence::all()->where('category', $category)->whereYear('created_at', $now->year)->count();

        return response()->json($categoryCount, 200);

    }
}
