<?php

namespace App\Http\Controllers\Student;

use App\Model\Student\Offence;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class OffenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $cases = Offence::all()->sortByDesc('created_at', 3);

        if ($cases != null ) {
            return response()->json($cases, 200);
        } else {
            return response()->json('Not found', 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $case = new Offence($request->all());

        if ($user->offence()->save($case)) {
            return response()->json('Case filed', 200);
        } else {
            return response()->json('Case filing failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $case = Offence::find($id);

        if ($case != null ) {
            return response()->json($case, 200);
        } else {
            return response()->json('not found', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $case =  Offence::find($id);

        if ($case != null ) {
            if ($case->update($request->all())){
                return response()->json('case updated', 200);
            } else {
                return response()->json('update failed', 200);
            }
        } else {
            return response()->json('Case not found', 404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
