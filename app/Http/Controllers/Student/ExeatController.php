<?php

namespace App\Http\Controllers\Student;

use App\Model\Student\Exeat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class ExeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $exeats = Exeat::all()->sortByDesc('created_at', 3);

        if ($exeats != null ) {
            return response()->json($exeats, 200);
        } else {
            return response()->json('No data found', 204);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $exeat = new Exeat($request->all());

        if ($user->exeat()->save($exeat)) {
            return response()->json('Exeat application successful', 200);
        } else {
            return response()->json('Application failed', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $exeat = Exeat::find($id);

        if ($exeat != null ) {
            return response()->json($exeat, 200);
        } else {
            return response()->json('Application not found', 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $exeat = Exeat::find($id);

        if ($exeat != null ){
           if ($exeat->update($request->all())) {
               return response()->json('Update applied', 200);
           } else {
               return response()->json('Update failed', 400);
           }
        } else {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
