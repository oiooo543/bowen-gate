<?php

namespace App\Http\Controllers\Student;

use App\Model\Student\Exeat;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;

class ExeatSpecController extends Controller
{
    public function getExeatByMatric($mat_no) {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }


        $users = User::where('mat_no', $mat_no)->pluck('id');

        $userID = null;
       foreach ($users as $userid) {
           $userID = $userid;
       }

     //  return $userID;
        $exeats = Exeat::where('user_id', $userID)->get();

       //return $exeats;

        if ($exeats != null ) {
            return response()->json($exeats, 200);
        } else {
            return response()->json('No data found', 200);
        }
    }

    public function storeMedic(Request $request)
    {
        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        if ($user->type == 'doc') {
            return response()->json('You are not authorized', 404);
        }

        $exeat = new Exeat($request->all());

        if ($exeat->save($exeat)) {
            return response()->json('Exeat application successful', 200);
        } else {
            return response()->json('Application failed', 400);
        }
    }

    public function getPending() {

        if (! $user = JWTAuth::parseToken()->authenticate()){
            return response()->json('User not registered', 404);
        }

        $exeat = Exeat::all()->where('status', 'Pending')->sortByDesc('created_at', 3);

        if ($exeat != null ) {
            return response()->json($exeat, 200);
        } else {
            return response('No details found', 200);
        }

    }
}
