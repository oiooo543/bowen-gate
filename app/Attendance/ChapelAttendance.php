<?php

namespace App\Attendance;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ChapelAttendance extends Model
{
    protected $fillable = [ 'mat_no',  'semester' ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
