<?php

namespace App\Attendance;

use Illuminate\Database\Eloquent\Model;

class HostelAttendance extends Model
{
    protected $fillable = ['mat_no', 'hostel', 'semester'];


    public function user () {
        return $this->belongsTo('user');
    }
}
