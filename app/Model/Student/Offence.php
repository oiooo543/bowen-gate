<?php

namespace App\Model\Student;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Offence extends Model
{
    protected $fillable = ['mat_no', 'category', 'semester', 'nature', 'location', 'date', 'time', 'evidence', 'item', 'remark'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
