<?php

namespace App\Model\Student;

use Illuminate\Database\Eloquent\Model;

class CourseEnrol extends Model
{
    protected $fillable = ['courseone', 'coursetwo', 'coursethree', 'coursefour', 'coursefive'. 'coursesix', 'courseseven',
                            'courseeight', 'coursenine', 'courseten', 'semester'];
}
