<?php

namespace App\Model\Student;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Statment extends Model
{
    protected $fillable = ['case_id', 'statement',  'status', 'action'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
