<?php

namespace App\Model\Student;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Exeat extends Model
{
    protected $fillable = [ 'staff_id', 'reason', 'status', 'departure_date', 'arrival_date', 'exeat_type', 'destination_address', 'condition' ];

    public function user () {
        return $this->belongsTo(User::class);
    }
}
