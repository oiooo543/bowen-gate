<?php

namespace App\Security;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GateMovement extends Model
{
    protected $fillable = ['phone', 'direction', 'security_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
