<?php

namespace App\Security;

use Illuminate\Database\Eloquent\Model;

class SecurityRegistration extends Model
{

    protected $fillable = [

        'name', 'phone', 'role', 'passport', 'status', 'home_address', 'nok', 'nok_phone', 'nok_address', 'campus_address'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
