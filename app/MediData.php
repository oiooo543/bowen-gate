<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediData extends Model
{
    //

    protected $fillable = [ 'admitted', 'surgery', 'p_prescription', 'asthma', 'ear', 'sleep_dist', 'a_bleeding',
        'faint', 'epilepsy', 'migraine', 'jaundice', 'sicklecell', 'rindisjection', 'diabetes', 'arthritis', 'skin',
        'tuberculosis', 'deformity', 'allergy', 'fhistory', 'immune', 'mat_no'
    ];

    public function user(){
        return  $this->belongsTo(User::class);
    }
}
