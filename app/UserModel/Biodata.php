<?php

namespace App\UserModel;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $fillable = [
        'department', 'faculty', 'level', 'haddress', 'state', 'town', 'country','hostel', 'nextkin', 'nextkin_phone', 'age', 'dob', 'gender', 'mstatus', 'parent_no'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
