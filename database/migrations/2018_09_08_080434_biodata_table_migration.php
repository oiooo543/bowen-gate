<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BiodataTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('biodatas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('department');
            $table->string('faculty');
            $table->string('level');
            $table->string('hostel');
            $table->string('haddress');
            $table->string('country');
            $table->string('state');
            $table->string('town');
            $table->string('nextkin');
            $table->string('nextkin_phone');
            $table->string('age');
            $table->string('dob');
            $table->string('gender');
            $table->string('mstatus');
            $table->string('user_id');
            $table->string('parent_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('biodatas');
    }
}
