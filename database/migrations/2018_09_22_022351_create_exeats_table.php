<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exeats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('staff_id')->nullable();
            $table->string('exeat_type');
            $table->string('destination_address');
            $table->string('departure_date');
            $table->string('arrival_date');
            $table->string('reason');
            $table->string('condition')->default('undefined');
            $table->string('status')->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exeats');
    }
}
