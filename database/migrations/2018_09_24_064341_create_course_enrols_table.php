<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseEnrolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_enrols', function (Blueprint $table) {
            $table->increments('id');
            $table->string('courseone');
            $table->string('coursetwo');
            $table->string('coursethree');
            $table->string('coursefour');
            $table->string('coursefive');
            $table->string('coursesix');
            $table->string('courseseven');
            $table->string('courseeight');
            $table->string('coursenine');
            $table->string('courseten');
            $table->string('semester');
            $table->string('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_enrols');
    }
}
