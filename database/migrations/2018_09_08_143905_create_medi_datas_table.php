<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medi_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('admitted');
            $table->string('surgery');
            $table->string('p_prescription');
            $table->string('asthma');
            $table->string('ear');
            $table->string('sleep_dist');
            $table->string('a_bleeding');
            $table->string('faint');
            $table->string('epilepsy');
            $table->string('migraine');
            $table->string('jaundice');
            $table->string('sicklecell');
            $table->string('rindisjection');
            $table->string('diabetes');
            $table->string('arthritis');
            $table->string('skin');
            $table->string('tuberculosis');
            $table->string('deformity');
            $table->string('allergy');
            $table->string('fhistory');
            $table->string('immune');
            $table->string('user_id');
            $table->string('mat_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medi_datas');
    }
}
